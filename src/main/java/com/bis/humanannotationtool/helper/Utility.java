package com.bis.humanannotationtool.helper;

import org.springframework.http.HttpStatus;

import com.bis.humanannotationtool.vo.ApiResponse;
import com.bis.humanannotationtool.vo.Paging;

public class Utility {
	
	/**
	 * Sets the response body.s
	 *
	 * @param data the data
	 * @return the respose body
	 */
	public static <E> ApiResponse<E> setResponseBody(final E data, HttpStatus code, Paging page) {
		final ApiResponse<E> resposeBody = new ApiResponse<E>();
		resposeBody.setStatus(Constants.SUCCESS);
		resposeBody.setData(data);
		resposeBody.setCode(code);
		resposeBody.setPage(page);
		return resposeBody;
	}
	
	

}
