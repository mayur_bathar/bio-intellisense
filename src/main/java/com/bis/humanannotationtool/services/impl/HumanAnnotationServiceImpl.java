package com.bis.humanannotationtool.services.impl;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.bis.humanannotationtool.entity.HumanAnnotation;
import com.bis.humanannotationtool.services.HumanAnnotationService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class HumanAnnotationServiceImpl implements HumanAnnotationService{

	private static final Logger logger = LoggerFactory.getLogger(HumanAnnotationServiceImpl.class);
	
	@Override
	public List<HumanAnnotation> listAudioFiles() {
		logger.info("list audio files");
		List<HumanAnnotation> audioFileList = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		TypeReference<List<HumanAnnotation>> typeReference = new TypeReference<List<HumanAnnotation>>(){};
		InputStream inputStream = TypeReference.class.getResourceAsStream("/audio.json");
		try {
			audioFileList = mapper.readValue(inputStream,typeReference);
			logger.info("List Audio files displayed successfully!");
		} catch (IOException e){
			logger.error("Unable to list audio files: " + e.getMessage());
		}
		return audioFileList;
	}

	
}
