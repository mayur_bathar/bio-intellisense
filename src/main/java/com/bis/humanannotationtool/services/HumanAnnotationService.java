package com.bis.humanannotationtool.services;

import java.util.List;

import com.bis.humanannotationtool.entity.HumanAnnotation;

public interface HumanAnnotationService {

	public List<HumanAnnotation> listAudioFiles();
}
