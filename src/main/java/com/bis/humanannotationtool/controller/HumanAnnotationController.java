package com.bis.humanannotationtool.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bis.humanannotationtool.helper.Utility;
import com.bis.humanannotationtool.services.HumanAnnotationService;
import com.bis.humanannotationtool.vo.ApiResponse;



@RestController
@RequestMapping("/api/humanannotation")
public class HumanAnnotationController {
	
	  private static final Logger logger = LoggerFactory.getLogger(HumanAnnotationController.class);
	
	  @Autowired
	  private HumanAnnotationService humanAnnoatationService;
	  /**
	   * Get all Audio Files.
	   *
	   * @return the list
	   */
	  @GetMapping("/list")
	  public ApiResponse getAllAudioFiles() {
		logger.info("start getting all audio files");  
		return Utility.setResponseBody(humanAnnoatationService.listAudioFiles(),HttpStatus.OK,null);
	    //return humanAnnoatationService.listAudioFiles();
	  }

}
