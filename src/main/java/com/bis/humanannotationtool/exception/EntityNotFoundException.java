package com.bis.humanannotationtool.exception;

public class EntityNotFoundException extends Exception {

	private String username;

	public static EntityNotFoundException createWith(String username) {
		return new EntityNotFoundException(username);
	}

	private EntityNotFoundException(String username) {
		this.username = username;
	}

	@Override
	public String getMessage() {
		return "User '" + username + "' not found";
	}

}
