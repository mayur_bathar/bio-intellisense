package com.bis.humanannotationtool.vo;

import org.springframework.http.HttpStatus;

public class ApiResponse<E> {

	private Object data;

	private String status;

	private HttpStatus code;

	private Paging page;
	
	public ApiResponse() {
	}
	
	public ApiResponse(Object data, String status, HttpStatus code, Paging page) {
		super();
		this.data = data;
		this.status = status;
		this.code = code;
		this.page = page;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public HttpStatus getCode() {
		return code;
	}

	public void setCode(HttpStatus code) {
		this.code = code;
	}

	public Paging getPage() {
		return page;
	}

	public void setPage(Paging page) {
		this.page = page;
	}
}
