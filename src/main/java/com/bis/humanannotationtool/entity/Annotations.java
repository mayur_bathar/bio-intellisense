package com.bis.humanannotationtool.entity;


public class Annotations {

	private Long audioTypeId;
	
	private String audioTypeValue;

	public Long getAudioTypeId() {
		return audioTypeId;
	}

	public void setAudioTypeId(Long audioTypeId) {
		this.audioTypeId = audioTypeId;
	}

	public String getAudioTypeValue() {
		return audioTypeValue;
	}

	public void setAudioTypeValue(String audioTypeValue) {
		this.audioTypeValue = audioTypeValue;
	}
}
