package com.bis.humanannotationtool.entity;

import java.util.Date;
import java.util.List;

public class HumanAnnotation {

	private String fileName;

	private Boolean isMarkedAnnotated;

	private String originalFileName;

	private String url;

	private Date createdAt;

	private Date updatedAt;

	private List<Segmentations> segmentations;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Boolean getIsMarkedAnnotated() {
		return isMarkedAnnotated;
	}

	public void setIsMarkedAnnotated(Boolean isMarkedAnnotated) {
		this.isMarkedAnnotated = isMarkedAnnotated;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<Segmentations> getSegmentations() {
		return segmentations;
	}

	public void setSegmentations(List<Segmentations> segmentations) {
		this.segmentations = segmentations;
	}
}
